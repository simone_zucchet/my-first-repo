#
# Cookbook:: nfv-apu-prov-sync
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

# Initialisation of attributes
apu_state_file = node['apu-prov-sync']['state_file_dir']
apu_id = node['hostname']
databag = "serviceGateways"
databag_item = "bag-settings-#{apu_id}"
apu_data_bag = data_bag_item(databag, databag_item)
databag_prov_state = apu_data_bag['provisioned_state']
# provisioning_url = "#{node['apu-prov-sync']['provision_url']}#{apu_id}"			# Reads APU id and creates URL for attributes
# deprovisioning_url = "#{node['apu-prov-sync']['deprovision_url']}#{apu_id}"		# Reads APU id and creates URL for attributes
provisioning_url = apu_data_bag['servicelist'][0]['push-job-enable']			# Reads full URL from Databag
deprovisioning_url = apu_data_bag['servicelist'][0]['push-job-disable']			# Reads full URL from Databag

chefapi_user = node['apu-prov-sync']['chefapi_user']
chefapi_password = node['apu-prov-sync']['chefapi_password']
apu_configuration_state = "default"

# Troubleshooting logs 
Chef::Log.warn "The APU ID = #{apu_id}"
Chef::Log.warn "The Databag provisioned state = [#{databag_prov_state}]"
Chef::Log.warn "The provisioning URL = #{provisioning_url}"
Chef::Log.warn "The deprovisioning URL = #{deprovisioning_url}"

if apu_data_bag['apu_type'] == "demo"
	Chef::Log.warn "The APU type is DEMO"
	Chef::Log.warn "Configuring APU integration with Demo Chef-API"
	chefapi_url = node['apu-prov-sync']['demo']['chefapi_url']
else
	Chef::Log.warn "Configuring APU integration with Dev Chef-API"
	chefapi_url = node['apu-prov-sync']['dev']['chefapi_url']
end

# Check to ensure that provisioned status exists in the APU
if File.exists?("#{apu_state_file}")
	Chef::Log.warn "APU status file exists at #{apu_state_file}. Reading provisioning status"
	# If the  file exists then read the file contents to determine configured status
	if File.exists?("#{apu_state_file}")
		File.open("#{apu_state_file}", "r") do |f|
			f.each_line do |line|
				apu_configuration_state = line.strip
			end
		end
	end

	# Confirmation of APU Configuration state
	Chef::Log.warn "The APU configuration state = [#{apu_configuration_state}]"

	# Function to compare APU configuration to Chef Databag
	if apu_configuration_state == databag_prov_state then
		# If Status matches do nothing
		Chef::Log.warn "APU provisioned status matched Chef databag - Do nothing"
	else
		Chef::Log.warn "APU provisioned status did not match Chef databag - Updating APU"
		# If status does not match, run required HTTPS request to match APU status to Databag status
		if databag_prov_state == "provisioned"
			Chef::Log.warn "Provisioning the APU"
			http_request 'Provisioning APU services' do
				# url "#{chefapi_url}/#{provisioning_url}/#{apu_id}"	# To be used when databag command is updated to remove id
				url "#{chefapi_url}/#{provisioning_url}"
				headers({'AUTHORIZATION' => "Basic #{Base64.encode64("#{chefapi_user}:#{chefapi_password}")}"})
			end
		elsif databag_prov_state == "deprovisioned"
			Chef::Log.warn "Deprovisioning the APU"
			http_request 'Deprovisioning APU services' do
				# url "#{chefapi_url}/#{deprovisioning_url}/#{apu_id}"	# To be used when databag command is updated to remove id
				url "#{chefapi_url}/#{deprovisioning_url}"
				headers({'AUTHORIZATION' => "Basic #{Base64.encode64("#{chefapi_user}:#{chefapi_password}")}"})
			end
		else
			# If state is not recognised raise the above
			Chef::Log.warn "Unable to recognise provisioned state for APU ID #{apu_id}, please check databg"
		end
	end

else
	Chef::Log.warn "The APU configuration file was not found at #{apu_state_file}"
	# Run the appropriate provisioning/deprovisioning command to sync APU with databag
	if databag_prov_state == "provisioned"
		Chef::Log.warn "Provisioning the APU"
		http_request 'Provisioning APU services' do
			# url "#{chefapi_url}/#{provisioning_url}/#{apu_id}"	# To be used when databag command is updated to remove id
			url "#{chefapi_url}/#{provisioning_url}"
			headers({'AUTHORIZATION' => "Basic #{Base64.encode64("#{chefapi_user}:#{chefapi_password}")}"})
		end
	elsif databag_prov_state == "deprovisioned"
		Chef::Log.warn "Deprovisioning the APU"
		http_request 'Deprovisioning APU services' do
			# url "#{chefapi_url}/#{deprovisioning_url}/#{apu_id}"	# To be used when databag command is updated to remove id
			url "#{chefapi_url}/#{deprovisioning_url}"
			headers({'AUTHORIZATION' => "Basic #{Base64.encode64("#{chefapi_user}:#{chefapi_password}")}"})
		end
	else
		# If state is not recognised raise the above
		Chef::Log.warn "Unable to recognise provisioned state for APU ID #{apu_id}, please check databg"
	end
end

# List of directories to be created
directories = [
	"opt/chameleon/info",
	"opt/chameleon/bin"
]

directories.each do |directory|
	directory "#{directory}" do
		action :create
	end
end

# Deploying OVs flow monitor services
Chef::Log.warn "Deploying APU flow monitor services"

# Creates the OVS Flow Monitor configuration file using template function
Chef::Log.warn "Deploying ovsFlowMon-config.json file"
template "opt//chameleon//info//ovsFlowMon-config.json" do
    action :create
    source "ovsFlowMon-config.json.erb"
	variables()
end

# Creates the OVS Flow Monitor services file using template function
Chef::Log.warn "Deploying ovsflowmon.service file"
cookbook_file 'etc//systemd//system//ovsflowmon.service' do
	source "ovsflowmon.service"
	mode '0700'
	action :create
end

# Creates the OVS Flow Monitor python script using template function
Chef::Log.warn "Deploying ovsFlowMon.py file"
cookbook_file 'opt//chameleon//bin//ovsFlowMon.py' do
	source "ovsFlowMon.py"
	mode '0700'
	action :create
	notifies :run, 'execute[enable-ovs-flow-monitor-service]', :immediately
end

# Enable the SSHD service to load new configuration
execute 'enable-ovs-flow-monitor-service' do
	user 'root'
	command 'systemctl enable ovsflowmon.service'
	action :nothing
	notifies :run, 'execute[start-ovs-flow-monitor-service]', :immediately
end

# Start the SSHD service to load new configuration
execute 'start-ovs-flow-monitor-service' do
	user 'root'
	command 'systemctl start ovsflowmon.service'
	action :nothing
end
